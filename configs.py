import json
import os

import boto3

# overall
SERVICE_NAME = 'distillery'
cwd = os.getcwd()
pos = cwd.find(SERVICE_NAME)
if pos > 0:
    PACKAGE_DIR = cwd[:cwd.find(SERVICE_NAME)] + SERVICE_NAME
else:
    PACKAGE_DIR = cwd + '/' + SERVICE_NAME


# AWS secret manager
AWS_REGION_NAME = "us-east-2"


def get_secret(secret_name):
    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=AWS_REGION_NAME
    )

    get_secret_value_response = client.get_secret_value(
        SecretId=secret_name
    )

    secret = get_secret_value_response['SecretString']

    return secret


# download files from S3 if needed
BUCKET_NAME = 'gatherscope-distillery'
s3 = boto3.resource('s3')

dirnames = [
    PACKAGE_DIR+'/data'
]
for dirname in dirnames:
    if not os.path.exists(dirname):
        os.mkdir(dirname)

filenames = [
    'data/recently_clustered_content.pkl',
    'data/presumm_checkpoint.pt'
]
for filename in filenames:
    if not os.path.exists(PACKAGE_DIR+'/'+filename):
        s3.Bucket(BUCKET_NAME).download_file(filename,
                                             PACKAGE_DIR+'/'+filename)


# MongoDB Connection
MDB_CREDS = json.loads(get_secret('mongodb/distillery/creds'))
MDB_USERNAME = MDB_CREDS['username']
MDB_PASSWORD = MDB_CREDS['password']
MDB_HOST = (("mongodb+srv://%s:%s@gatherscope-us-gg0fy"
             + ".mongodb.net/test?retryWrites=true")
            % (MDB_USERNAME, MDB_PASSWORD))

# summarization params


class DotDict(dict):
    """dot.notation access to dictionary attributes"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


CLUSTERS_COLLECTION_HOURS = 6
ARTICLES_COLLECTION_DAYS = 8
SUMMARIZATION_RUN_INTERVAL_MINUTES = 1
SUMMARY_MAX_SENTENCES_TO_CONSIDER = 10000
SUMMARY_N_SENTENCES = 2
SUMMARY_MIN_SENT_CHARS = 100
SUMMARY_MAX_SENT_CHARS = 200
CURSE_TERMS = (
    '\b(ass|asshole|bastard|bastards|bitch|bitches|bitchier|bitchiest|'
    + 'bitching|bitchy|cock|cocks|cocksucker|cocksuckers|cunt|cunts|cunty|'
    + 'fag|fags|faggot|faggots|faggy|fuck|fucker|fucking|fucks|motherfuck|'
    + 'motherfucker|motherfuckers|motherfucking|nigga|niggas|nigger|niggers|'
    + 'piss|pissed|pisser|pisses|pissing|shit|shitter|shittier|shittiest|'
    + 'shits|shitty|twat|twats)\b'
 )
IMAGE_URLS_BLACKLIST = [
    'https://nation.com.pk/assets/thenation/images/no-image-large.jpg'
]
PRESUMM_ARGS = {
    'test_from': './data/presumm_checkpoint.pt',
    'large': False,
    'temp_dir': '../temp',
    'finetune_bert': True,
    'encoder': 'bert',
    'enc_layers': 6,
    'enc_hidden_size': 512,
    'enc_ff_size': 512,
    'enc_dropout': 0.2,
    'max_pos': 512,
    'share_emb': False,
    'dec_layers': 6,
    'dec_hidden_size': 768,
    'dec_ff_size': 2048,
    'dec_heads': 8,
    'dec_dropout': 0.2,
    'use_bert_emb': False,
    'min_src_ntokens_per_sent': 5,
    'max_src_ntokens_per_sent': 200,
    'min_src_nsents': 3,
    'max_src_nsents': 100,
    'beam_size': 3,
    'min_length': 15,
    'max_length': 150,
    'alpha': 0.6,
    'block_trigram': True
}
PRESUMM_ARGS = DotDict(PRESUMM_ARGS)
PRESUMM_DEVICE = 'cpu'
SORT_SUMMARY_CHRONOLOGICALLY = False
STRIP_TERMS = [
    'Breaking News Emails Get breaking news alerts and special reports. \
        The news and stories that matter, delivered weekday mornings.',
    'Get celebs updates directly to your inbox Subscribe Thank you for \
        subscribing We have more newsletters Show me See our privacy \
        notice Could not subscribe, try again later Invalid Email\n\n',
    'This material may not be published, broadcast, rewritten or \
        redistributed.'
]
