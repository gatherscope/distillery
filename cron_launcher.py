from content_summarizer import enqueue_content_for_summarization

if __name__ == "__main__":
    enqueue_content_for_summarization()
