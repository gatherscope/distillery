from operator import itemgetter
import pickle
import time
import unittest

import nltk
from nltk import sent_tokenize
import numpy as np

from collection import get_recently_clustered_content
from content_summarizer import pagerank, mmr_summarize
from utils import clean, get_best_image, include, is_url_jpeg, simple_split, \
                  vectorize

nltk.download('punkt')


class UtilityMethods(unittest.TestCase):
    def setUp(self):
        self.startTime = time.time()

        with open('data/recently_clustered_content.pkl', 'rb') as f:
            self.recently_clustered_content = pickle.load(f)

    def tearDown(self):
        t = time.time() - self.startTime
        print("%s: %.3f" % (self.id(), t))

    def test_clean(self):
        text = (
            "\n\n On Tuesday,the U.S. state department issued a statement "
            + "in support of the U.K.'s request to leave the E.U. after an 8 "
            + "week delay. . "
        )
        actual_cleaned = clean(text)
        intended_cleaned = (
            " On Tuesday, the US state department issued a statement "
            + "in support of the UK's request to leave the EU after an 8 "
            + "week delay. "
        )
        self.assertEqual(actual_cleaned, intended_cleaned)

    def test_get_best_image(self):
        cluster_content = self.recently_clustered_content[0]
        url = get_best_image(cluster_content['images'])
        self.assertTrue(isinstance(url, str))

    def test_include(self):
        text = 'On Tuesday,the U.S. state department issued a statement.'
        self.assertTrue(include(text))

        text = 'On Tuesday,'
        self.assertFalse(include(text))

        text = 'Pictured: the U.S. state department.'
        self.assertFalse(include(text))

        text = '(the U.S. state department)'
        self.assertFalse(include(text))

    def test_is_url_jpeg(self):
        url = (
            "https://s3.amazonaws.com/gatherscope-people/gs_logo_email_sig.png"
        )
        self.assertFalse(is_url_jpeg(url))

    def test_simple_split(self):
        text = 'This is true.'
        actual_split = simple_split(text)
        intended_split = ['This', 'is', 'true.']
        self.assertEqual(actual_split, intended_split)

    def test_vectorize(self):
        cluster_content = self.recently_clustered_content[0]
        vectors, idx_2_token = vectorize(cluster_content['texts'])
        self.assertEqual(len(vectors.shape), 2)
        self.assertEqual(idx_2_token[-1][0], 'z')


class LoadingMethods(unittest.TestCase):
    def setUp(self):
        self.startTime = time.time()

    def tearDown(self):
        t = time.time() - self.startTime
        print("%s: %.3f" % (self.id(), t))

    def test_get_recently_clustered_content(self):
        recently_clustered_content = get_recently_clustered_content(
            clusters_collection_hours=4,
            articles_collection_days=8,
            write_in_progress=False,
            generated_summary_filt=None
        )
        self.assertEqual(
            list(recently_clustered_content[0].keys()),
            ['_id', 'texts', 'titles', 'urls', 'publishers', 'images']
        )


class SummarizationMethods(unittest.TestCase):
    def setUp(self):
        self.startTime = time.time()

        with open('data/recently_clustered_content.pkl', 'rb') as f:
            self.recently_clustered_content = pickle.load(f)

    def tearDown(self):
        t = time.time() - self.startTime
        print("%s: %.3f" % (self.id(), t))

    def test_pagerank(self):
        connectivity = np.array([
            [0.1, 0.9, 0.0],
            [0.33, 0.33, 0.34],
            [0.5, 0.25, 0.25]
        ])

        probs = pagerank(connectivity)

        self.assertEqual(np.round(probs[0], 3), 0.305)

    def test_mmr_summarize(self):
        cluster_content = self.recently_clustered_content[0]
        texts = cluster_content['texts']

        texts_sentences = [sent_tokenize(clean(text)) for text in texts]

        combined_sentences = []
        sentence_text_positions = {}

        for text_sentences in texts_sentences:
            n_sentences = len(text_sentences)
            for idx, sentence in enumerate(text_sentences):
                if include(sentence):
                    combined_sentences.append(sentence)
                    sentence_text_positions[sentence] = idx/n_sentences

        combined_sentences_vectors, _ = vectorize(combined_sentences)
        non_empty_sentence_indexes = [
            idx for idx, vector in enumerate(combined_sentences_vectors)
            if np.sum(vector) > 0
        ]

        combined_sentences = (
            itemgetter(*non_empty_sentence_indexes)(combined_sentences)
        )
        combined_sentences_vectors = (
            combined_sentences_vectors[non_empty_sentence_indexes]
        )

        summary_sentences = mmr_summarize(
            combined_sentences, combined_sentences_vectors,
            n_sentences=2,
            min_sent_len=100,
            max_sent_len=300
        )

        self.assertEqual(
            summary_sentences[0],
            ("The White House escalated its assault on the Muslim American "
             + "congresswoman Ilhan Omar on Sunday, after Donald Trump "
             + "repeatedly tweeted video footage of September 11 and accused "
             + "Omar of downplaying the terror attacks.")
        )

        # TODO: add presumm tests


if __name__ == '__main__':
    unittest.main()
