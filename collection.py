from datetime import datetime, timedelta

from bson.objectid import ObjectId
from pymongo import MongoClient

import configs

# MongoDB Config
MDB_CLIENT = MongoClient(configs.MDB_HOST)
MILLI_MDB = MDB_CLIENT.milli_mdb
PARSED_CONTENT = MILLI_MDB.parsed_content
SORTINGHAT_MDB = MDB_CLIENT.sortinghat_mdb
CONTENT_CLUSTERS = SORTINGHAT_MDB.content_clusters


def get_recently_clustered_content(clusters_collection_hours=12,
                                   articles_collection_days=8,
                                   write_in_progress=True,
                                   generated_summary_filt={'$exists': False}):
    """Gets data and metadata of recently-crawled publisher content from Mongo.

    Keyword arguments:
    clusters_collection_hours -- lookback window for cluster querying
        (int, default: 12)
    articles_collection_days -- lookback window for article (cluster members)
        querying (int, default: 8)

    Returns:
    list -- in the form [cluster_content_1, cluster_content_2, ...] where
        cluster_content objects are dicts in the form {'_id': str(i), 'titles':
        [ti], 'texts': [te], 'urls': {ur}, 'publishers': {pb}, 'images': {im}},
        where [ti] and [te] are lists of strings and {ur}, {pb}, and {im} are
        dicts of strings
    """
    clusters_start_timestamp = (datetime.utcnow()
                                - timedelta(hours=clusters_collection_hours))

    query = {'batch_timestamp': {'$gt': clusters_start_timestamp}}
    if generated_summary_filt:
        query['generated_summary'] = generated_summary_filt
    recent_clusters_cursor = CONTENT_CLUSTERS.find(query)

    recently_clustered_content = []
    for idx, cluster in enumerate(recent_clusters_cursor):
        if write_in_progress:
            CONTENT_CLUSTERS.update_one(
                {'_id': cluster['_id']},
                {'$set': {
                    'generated_summary': 'in progress'}},
                upsert=False
            )
        members = cluster['member_ids'].split(', ')
        members = [ObjectId(member) for member in members]

        articles_start_timestamp = (
            str(datetime.utcnow()
                - timedelta(articles_collection_days))
        )
        content_cursor = PARSED_CONTENT.find(
            {'crawled_timestamp': {'$gt': articles_start_timestamp},
             '_id': {'$in': members},
             }
        )

        texts = []
        titles = []
        urls = {}
        publishers = {}
        images = {}
        for article in content_cursor:
            texts += [article['text']]
            title = article['title']
            titles += [title]
            urls[title] = article['url']
            publishers[title] = article['publisher']
            images[title] = article['top_image']

        recently_clustered_content.append({
            '_id': cluster['_id'],
            'texts': texts,
            'titles': titles,
            'urls': urls,
            'publishers': publishers,
            'images': images,
        })

    return recently_clustered_content
