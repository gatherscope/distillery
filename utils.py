import re
import requests

from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer

import configs


def clean(txt):
    """Scrubs unwanted text from a string (determined in configs).

    Keyword arguments:
    txt -- string from which to strip unwanted text (string)

    Returns:
    string -- cleaned txt
    """
    strip_terms = configs.STRIP_TERMS
    for term in strip_terms:
        if term in txt:
            txt = txt.replace(term, '')
    txt = re.sub('\n', ' ', txt)
    txt = re.sub('U.S.A.', 'USA', txt)  # sent_tokenize struggles with this
    txt = re.sub('U.S.', 'US', txt)  # sent_tokenize struggles with this
    txt = re.sub('U.K.', 'UK', txt)  # sent_tokenize struggles with this
    txt = re.sub('E.U.', 'EU', txt)  # sent_tokenize struggles with this
    # correct punctuation not followed by space, not decimal
    txt = re.sub(r'(?<=[.,!?])(?=[^\s0-9])', ' ', txt)
    txt = re.sub(r'\s+[.!?,:;]\s+', ' ', txt)  # remove floating punctuation
    txt = re.sub(r'\s{2,}', ' ', txt)  # remove multiple whitespace

    return txt


def get_best_image(images, best_title=None):
    """Select best image given title-image_url pairs.

    Given a dict of title-image_url pairs, and possibly a best title whose
        image url to check first, iterate through the urls and return the first
        one that is a live jpeg image and which does not contain any of the
        strings in configs.IMAGE_URLS_BLACKLIST.

    Keyword arguments:
    images -- dict of title-image_url pairs (dict)
    best_title -- title of article whose image_url should be checked first
        (string, optional)

    Returns:
    string -- jpeg image url
    """
    image_urls = []

    # if best_title given, check this title's image first
    if best_title and best_title in images:
        image_url = images[best_title]
        image_urls.append(image_url)

    for image_url in images.values():
        image_urls.append(image_url)

    for image_url in image_urls:
        return_url = is_url_jpeg(image_url)
        for blacklist_str in configs.IMAGE_URLS_BLACKLIST:
            return_url = (return_url and not (blacklist_str in image_url))
        if return_url:
            return image_url

    return image_urls[0]  # if no image is jpeg, return the first url


def include(txt, min_len=25, max_len=1e6):
    """Return whether to include a string.

    Keyword arguments:
    txt -- string to be considered (string)
    min_len -- if len(text) < this, don't include (int, default: 25)
    max_len -- if len(text) > this, don't include (int, default: 1e6)

    Returns:
    bool -- whether to include txt
    """
    include = (
        (len(txt) >= min_len) and (len(txt) <= max_len)
        # discard credits, links, photos
        and not re.search(
            r'(click|credit:|http|pictured:|photo:|more »)',
            txt,
            re.IGNORECASE
        )
        # discard parentheticals
        and not re.search(r'^\(', txt, re.IGNORECASE)
    )
    return include


def is_url_jpeg(image_url):
    """Check whether an image_url points to a live jpeg image.

    Keyword arguments:
    image_url -- url to check (string)

    Returns:
    bool -- whether a live jpeg is at the image_url
    """

    try:
        image_formats = ("image/jpeg", "image/jpg")
        r = requests.head(image_url, timeout=0.5)
        return r.headers["content-type"] in image_formats
    except:
        return False


def simple_split(txt):
    """Splits txt by spaces (' ').

    Keyword arguments:
    txt -- string to be split

    Returns:
    list of words
    """
    return txt.split(' ')


def tile(x, count, dim=0):
    """
    Tiles x on dimension dim count times.
    """
    perm = list(range(len(x.size())))
    if dim != 0:
        perm[0], perm[dim] = perm[dim], perm[0]
        x = x.permute(perm).contiguous()
    out_size = list(x.size())
    out_size[0] *= count
    batch = x.size(0)
    x = x.view(batch, -1) \
         .transpose(0, 1) \
         .repeat(count, 1) \
         .transpose(0, 1) \
         .contiguous() \
         .view(*out_size)
    if dim != 0:
        x = x.permute(perm).contiguous()
    return x


def vectorize(docs, tfidf=False):
    """Given some docs, transforms them into bag-of-words vector form.

    (optional TF-IDF conversion in post)

    Keyword arguments:
    docs -- docs to transform (list of strings)
    tfidf -- whether to transform count bag-of-words vectorization into tfidf
        (bool, default: True)

    Returns:
    sparse array -- docs in vectorized form
    list of string -- the tokens vocabulary corresponding to the docs and
        vectorization
    """
    # get initial vocab, vectors, embeddings
    count_vectorizer = CountVectorizer(
        ngram_range=(1, 2), min_df=1,
        max_df=1.0, stop_words='english'
    )
    doc_vectors_count = count_vectorizer.fit_transform(docs)
    idx_2_token = count_vectorizer.get_feature_names()

    if tfidf:
        tfidf_transformer = TfidfTransformer(norm='l2', use_idf=True)
        doc_vectors_tfidf = tfidf_transformer.fit_transform(doc_vectors_count)
        return doc_vectors_tfidf, idx_2_token
    return doc_vectors_count, idx_2_token
