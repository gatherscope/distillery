from datetime import datetime, timezone
import logging
from operator import itemgetter
import os
import re

import numpy as np
import nltk
from nltk import sent_tokenize, word_tokenize
from pymongo import MongoClient
from pytorch_transformers import BertTokenizer
from redis import Redis
from rq import Queue
from scipy.sparse import vstack
from sklearn.metrics.pairwise import cosine_similarity
import torch

from collection import get_recently_clustered_content
import configs
from models.model_builder import AbsSummarizer
from utils import clean, get_best_image, include, tile, vectorize

nltk.download('punkt')

# MongoDB Config
MDB_CLIENT = MongoClient(configs.MDB_HOST)
SORTINGHAT_MDB = MDB_CLIENT.sortinghat_mdb
CONTENT_CLUSTERS = SORTINGHAT_MDB.content_clusters

# Redis Config
REDIS_CONN = Redis()
REDIS_QUEUE = Queue(connection=REDIS_CONN)


# Logging Config
if not os.path.exists('logs'):
    os.mkdir('logs')
logging.basicConfig(
    filename='logs/performance.log',
    format='%(asctime)s %(message)s',
    level=logging.DEBUG
)

# Load presumm model
PRESUMM_ARGS = configs.PRESUMM_ARGS
PRESUMM_DEVICE = configs.PRESUMM_DEVICE
checkpoint = torch.load(
    PRESUMM_ARGS.test_from,
    map_location=lambda storage, loc: storage
)

MODEL_FLAGS = [
    'hidden_size', 'ff_size', 'heads', 'emb_size', 'enc_layers',
    'enc_hidden_size', 'enc_ff_size', 'dec_layers', 'dec_hidden_size',
    'dec_ff_size', 'encoder', 'ff_actv', 'use_interval'
]

opt = vars(checkpoint['opt'])
for k in opt.keys():
    if (k in MODEL_FLAGS):
        setattr(PRESUMM_ARGS, k, opt[k])

presumm_model = AbsSummarizer(PRESUMM_ARGS, PRESUMM_DEVICE, checkpoint)
presumm_model.eval()

presumm_tokenizer = BertTokenizer.from_pretrained(
    'bert-base-uncased',
    do_lower_case=True,
    cache_dir=PRESUMM_ARGS.temp_dir
)
symbols = {
    'BOS': presumm_tokenizer.vocab['[unused0]'],
    'EOS': presumm_tokenizer.vocab['[unused1]'],
    'PAD': presumm_tokenizer.vocab['[PAD]'],
    'EOQ': presumm_tokenizer.vocab['[unused2]']
}
PRESUMM_ARGS.cls_token = '[CLS]'
PRESUMM_ARGS.sep_token = '[SEP]'
PRESUMM_ARGS.pad_token = '[PAD]'
PRESUMM_ARGS.cls_vid = presumm_tokenizer.vocab[PRESUMM_ARGS.cls_token]
PRESUMM_ARGS.sep_vid = presumm_tokenizer.vocab[PRESUMM_ARGS.sep_token]
PRESUMM_ARGS.pad_vid = presumm_tokenizer.vocab[PRESUMM_ARGS.pad_token]


def enqueue_content_for_summarization():
    """
    Queries mongodb for content to be summarized.

    Keyword arguments:
    None

    Returns:
    None, but enqueues into rq a mdb_write_summary job
    """

    collection_timestamp = datetime.utcnow().replace(tzinfo=timezone.utc)
    outstr = 'Collection begun at %s' % str(collection_timestamp)
    print(outstr)
    logging.info(outstr)

    recently_clustered_content = get_recently_clustered_content(
        configs.CLUSTERS_COLLECTION_HOURS,
        configs.ARTICLES_COLLECTION_DAYS
    )

    queueing_timestamp = datetime.utcnow().replace(tzinfo=timezone.utc)
    outstr = 'Queueing begun at %s' % str(queueing_timestamp)
    print(outstr)
    logging.info(outstr)

    n_clusters = 0
    if recently_clustered_content:
        for cluster_content in recently_clustered_content:
            n_clusters += 1
            if n_clusters % 100 == 0:
                print('%d clusters iterated' % n_clusters)
            REDIS_QUEUE.enqueue(
                mdb_write_summary, cluster_content, timeout=120, ttl=-1
            )

    outstr = ('%d clusters queued' % n_clusters)
    print(outstr)
    logging.info(outstr)


def mdb_write_summary(cluster_content):
    """Summarizes cluster_content and writes result to Mongo.

    Given a cluster_content object, determines best title and generates a
        summary from the member articles. Currently summarization is achieved
        in two stages: extractive top sentence selection via MMR and
        abstractive summarization of top sentences via presumm.

    Keyword arguments:
    cluster_content -- dict in the form {'_id': str(i), 'titles': [ti],
        'texts': [te], 'urls': {ur}, 'publishers': {pb}, 'images': {im}}, where
        [ti] and [te] are lists of strings and {ur}, {pb}, and {im} are dicts
        of strings (dict)

    Returns:
    None
    """
    record_id = cluster_content['_id']
    texts = cluster_content['texts']
    titles = cluster_content['titles']
    urls = cluster_content['urls']
    publishers = cluster_content['publishers']
    images = cluster_content['images']

    # find most representative title
    title_vectors, _ = vectorize(titles)
    non_empty_title_indexes = [
        idx for idx, (title, vector) in enumerate(zip(titles, title_vectors))
        if (np.sum(vector) > 0 and include(title, min_len=0))
    ]

    if len(non_empty_title_indexes) > 1:
        titles = itemgetter(*non_empty_title_indexes)(titles)
        title_vectors = title_vectors[non_empty_title_indexes]
        best_title = mmr_summarize(
            titles,
            title_vectors,
            n_sentences=1,
            skip_filters=True
        )[0]
        best_url = urls[best_title]
        best_publisher = publishers[best_title]
        best_image = get_best_image(images, best_title)
    else:  # fallback if no titles vectorizable
        best_title = titles[0]
        best_url = urls[best_title]
        best_publisher = publishers[best_title]
        best_image = get_best_image(images, best_title)

    # generate summary
    texts_sentences = [sent_tokenize(clean(text)) for text in texts]
    combined_sentences = []
    sentence_text_positions = {}

    for text_sentences in texts_sentences:
        n_sentences = len(text_sentences)
        for idx, sentence in enumerate(text_sentences):
            if include(sentence):
                combined_sentences.append(sentence)
                sentence_text_positions[sentence] = idx/n_sentences

    combined_sentences = (
        combined_sentences[:configs.SUMMARY_MAX_SENTENCES_TO_CONSIDER]
    )

    if not combined_sentences:
        print('\nsummarization of cluster %s failed' % cluster_content['_id'])
        print('no text sentences available for summarization')
        return None

    combined_sentences_vectors, _ = vectorize(combined_sentences)
    non_empty_sentence_indexes = [
        idx for idx, vector in enumerate(combined_sentences_vectors)
        if np.sum(vector) > 0
    ]

    if non_empty_sentence_indexes:
        combined_sentences = (
            itemgetter(*non_empty_sentence_indexes)(combined_sentences)
        )
        combined_sentences_vectors = (
            combined_sentences_vectors[non_empty_sentence_indexes]
        )

        top_sentences = sorted(
            mmr_summarize(
                combined_sentences, combined_sentences_vectors,
                n_sentences=25
            ),
            key=lambda x: sentence_text_positions[x]
        )

        summary = presumm(' '.join(top_sentences))
        summary_sentences = [
            sent.capitalize()
            for sent
            in sent_tokenize(summary)
        ]
    else:
        print('\nsummarization of cluster %s failed' % cluster_content['_id'])
        print('no text sentences available for summarization')
        return None

    generated_summary = ''
    for sentence in summary_sentences:
        generated_summary += ('<li>%s</li>' % sentence)

    print('\nsummarization of cluster %s succeeded' % cluster_content['_id'])
    print('best title: %s' % best_title)
    print('generated summary: %s' % generated_summary)
    CONTENT_CLUSTERS.update_one(
        {'_id': record_id},
        {'$set': {
            'best_title': best_title,
            'best_url': best_url,
            'best_publisher': best_publisher,
            'best_image': best_image,
            'generated_summary': generated_summary}},
        upsert=False
    )


def mmr_summarize(sentences, sentences_vectors, n_sentences=3, L=0.75,
                  min_sent_len=0, max_sent_len=1e6, skip_filters=False):
    """Maximal marginal relevance algorithm for text summarization.

    Implementation of maximal marginal relevance algorithm for extractive text
        summarization, originally described here:
        https://dl.acm.org/citation.cfm?id=291025

    Keyword arguments:
    sentences -- sentence strings constituting the doc or docs to be summarized
        (list)
    sentences_vectors -- vector representation of sentences
        (matrix or list of vectors)
    n_sentences -- length of output summary (int, default: 3)
    L -- lambda parameter of algorithm (float, default: 0.5)
    min_sent_len -- output summary sentences must be at least this many chars
        (int, default: 0)
    max_sent_len -- output summary sentences may be at most this many chars
        (int, default: 1e6)
    skip_filters -- whether to skip discarding of problematic words, characters
        etc (bool, default: False)

    Returns:
    string -- bulleted summary of sentences
    """
    sentences_sim_matrix = cosine_similarity(sentences_vectors)
    # normalize row-wise
    sentences_sim_matrix /= np.sum(sentences_sim_matrix, axis=1)[:, None]

    sentences_pagerank_probs = pagerank(sentences_sim_matrix)
    sentences_sim_penalties = np.zeros(len(sentences_pagerank_probs))
    summary_sentences = []
    summary_sentences_vectors = None

    while len(summary_sentences) < n_sentences:
        sentences_scores = (
            L*sentences_pagerank_probs-(1-L)*sentences_sim_penalties
        )
        sentences_ordered_indexes = np.argsort(-sentences_scores)
        sentences_ordered = itemgetter(*sentences_ordered_indexes)(sentences)
        indexes_ordered = [
            idx for idx, sentence in zip(sentences_ordered_indexes,
                                         sentences_ordered)
            if (
                ((len(sentence) >= min_sent_len)
                 and (len(sentence) <= max_sent_len)
                 # sentence should start w/ capital letter
                 and re.search(r'^[A-Z].+', sentence)
                 # discard starts which require context
                 and not re.search(
                    r'^(and|but|she|he|hers|his|their|they|we|I)\b',
                    sentence,
                    re.IGNORECASE
                 )
                 # discard 'problematic characters
                 and not re.search(r'[\—\–\-\#\@\:\;\?\!\(\[]', sentence)
                 # discard quotes
                 and not re.search(r'[\“\"\”]|\bsaid\b', sentence)
                 # discard 'WASHINGTON', 'FILE PHOTO:' etc
                 and not re.search(r'[A-Z]{2,}', sentence)
                 and not re.search(
                    r'\b(advertisement|image|images|photo|photograph\
                        |pictured)\b',
                    sentence,
                    re.IGNORECASE
                 )
                 and not re.search(
                    configs.CURSE_TERMS,
                    sentence,
                    re.IGNORECASE
                 ))
                or skip_filters
            )
        ]

        if not indexes_ordered:  # edge case where all sentences filtered
            print('mmr skip filter triggered')
            return mmr_summarize(sentences, sentences_vectors, n_sentences, L,
                                 min_sent_len, max_sent_len, skip_filters=True)

        best_sent_idx = indexes_ordered[0]

        summary_sentences.append(sentences[best_sent_idx])

        if summary_sentences_vectors is None:
            # initial run
            summary_sentences_vectors = sentences_vectors[best_sent_idx]
        else:
            summary_sentences_vectors = vstack(
                [summary_sentences_vectors, sentences_vectors[best_sent_idx]]
            )
        sentences_sim_penalties = np.mean(
            cosine_similarity(sentences_vectors, summary_sentences_vectors),
            axis=1
        )
        sentences_sim_penalties /= sentences_sim_penalties.sum()

    return summary_sentences


def pagerank(A, eps=0.0001, d=0.85):
    """Simple implementation of pagerank.

    Simple implementation of pagerank algorithm described here:
        https://nlpforhackers.io/textrank-text-summarization/

    Keyword arguments:
    A -- matrix representing strength of connection between observations
        (matrix)
    eps -- epsilon parameter (float, default: 0.0001)
    d -- damping parameter (float, default: 0.85)

    Returns:
    vector -- pagerank vector P
    """
    P = np.ones(len(A)) / len(A)
    while True:
        new_P = (1 - d) * np.ones(len(A)) / len(A) + d * A.T.dot(P)
        delta = abs(new_P - P).sum()
        if delta <= eps:
            return new_P
        P = new_P


def presumm(txt):
    """Modified implementation of https://github.com/nlpyang/PreSumm"""
    src, segs, mask_src = tokenize_for_presumm(txt)

    src_features = presumm_model.bert(src, segs, mask_src)
    dec_states = presumm_model.decoder.init_decoder_state(
        src, src_features, with_cache=True
    )

    bs_results = presumm_beam_search(src_features, dec_states)

    summary_token_ids = [int(n) for n in bs_results['predictions'][0][0]]
    summary = presumm_tokenizer.decode(summary_token_ids)
    summary = (
        summary
        .replace(' ##', '')
        .replace(' [unused2] ', '. ')
        .replace(' [unused1]', '.')
    )

    del src_features, dec_states  # clear memory

    return summary


def tokenize_for_presumm(txt):
    src = [word_tokenize(sent) for sent in sent_tokenize(txt)]

    idxs = [
        i
        for i, s
        in enumerate(src)
        if len(s) > PRESUMM_ARGS.min_src_ntokens_per_sent
    ]

    src = [src[i][:PRESUMM_ARGS.max_src_ntokens_per_sent] for i in idxs]
    src = src[:PRESUMM_ARGS.max_src_nsents]

    src_txt = [' '.join(sent) for sent in src]
    text = (
        ' {} {} '
        .format(PRESUMM_ARGS.sep_token, PRESUMM_ARGS.cls_token)
        .join(src_txt)
    )

    src_subtokens = presumm_tokenizer.tokenize(text)[:510]

    src_subtokens = (
        [PRESUMM_ARGS.cls_token]
        + src_subtokens
        + [PRESUMM_ARGS.sep_token]
    )
    src_subtoken_idxs = presumm_tokenizer.convert_tokens_to_ids(src_subtokens)

    _segs = (
        [-1]
        + [i
           for i, t
           in enumerate(src_subtoken_idxs)
           if t == PRESUMM_ARGS.sep_vid]
    )
    segs = [_segs[i] - _segs[i - 1] for i in range(1, len(_segs))]
    segments_ids = []
    for i, s in enumerate(segs):
        if (i % 2 == 0):
            segments_ids += s * [0]
        else:
            segments_ids += s * [1]
    src_subtoken_idxs = torch.tensor(
        src_subtoken_idxs,
        device=PRESUMM_DEVICE
    ).unsqueeze(0)
    segments_ids = torch.tensor(
        segments_ids,
        device=PRESUMM_DEVICE
    ).unsqueeze(0)

    mask_src = 1 - (src_subtoken_idxs == PRESUMM_ARGS.pad_vid)

    return src_subtoken_idxs, segments_ids, mask_src


def presumm_beam_search(src_features, dec_states):
    batch_size = src_features.shape[0]

    # Tile states and memory beam_size times
    src_features = tile(src_features, PRESUMM_ARGS.beam_size, dim=0)
    dec_states.map_batch_fn(
        lambda state, dim: tile(state, PRESUMM_ARGS.beam_size, dim=dim)
    )
    batch_offset = torch.arange(
        batch_size, dtype=torch.long, device=PRESUMM_DEVICE
    )
    beam_offset = torch.arange(
        0,
        batch_size * PRESUMM_ARGS.beam_size,
        step=PRESUMM_ARGS.beam_size,
        dtype=torch.long,
        device=PRESUMM_DEVICE
    )
    alive_seq = torch.full(
        [batch_size * PRESUMM_ARGS.beam_size, 1],
        symbols['BOS'],
        dtype=torch.long,
        device=PRESUMM_DEVICE
    )

    # Give full probability to the first beam on the first step.
    topk_log_probs = torch.tensor(
        [0.0] + [float("-inf")] * (PRESUMM_ARGS.beam_size - 1),
        device=PRESUMM_DEVICE
    ).repeat(batch_size)

    # Structure that holds finished hypotheses.
    hypotheses = [[] for _ in range(batch_size)]  # noqa: F812

    results = {}
    results["predictions"] = [[] for _ in range(batch_size)]  # noqa: F812
    results["scores"] = [[] for _ in range(batch_size)]  # noqa: F812
    results["gold_score"] = [0] * batch_size

    for step in range(PRESUMM_ARGS.max_length):
        decoder_input = alive_seq[:, -1].view(1, -1)

        # Decoder forward.
        decoder_input = decoder_input.transpose(0, 1)

        dec_out, dec_states = presumm_model.decoder(
            decoder_input, src_features, dec_states, step=step
        )

        # Generator forward.
        log_probs = (
            presumm_model.generator.forward(dec_out.transpose(0, 1).squeeze(0))
        )
        vocab_size = log_probs.size(-1)

        if step < PRESUMM_ARGS.min_length:
            log_probs[:, symbols['EOS']] = -1e20

        # Multiply probs by the beam probability.
        log_probs += topk_log_probs.view(-1).unsqueeze(1)

        alpha = PRESUMM_ARGS.alpha
        length_penalty = ((5.0 + (step + 1)) / 6.0) ** alpha

        # Flatten probs into a list of possibilities.
        curr_scores = log_probs / length_penalty

        if(PRESUMM_ARGS.block_trigram):
            cur_len = alive_seq.size(1)
            if cur_len > 3:
                for i in range(alive_seq.size(0)):
                    fail = False
                    words = [int(w) for w in alive_seq[i]]
                    words = [presumm_tokenizer.ids_to_tokens[w] for w in words]
                    words = ' '.join(words).replace(' ##', '').split()
                    if len(words) <= 3:
                        continue
                    trigrams = [
                        (words[i-1], words[i], words[i+1])
                        for i in range(1, len(words)-1)
                    ]
                    trigram = tuple(trigrams[-1])
                    if trigram in trigrams[:-1]:
                        fail = True
                    if fail:
                        curr_scores[i] = -10e20

        curr_scores = curr_scores.reshape(
            -1, PRESUMM_ARGS.beam_size * vocab_size
        )

        topk_scores, topk_ids = curr_scores.topk(
            PRESUMM_ARGS.beam_size, dim=-1
        )

        # Recover log probs.
        topk_log_probs = topk_scores * length_penalty

        # Resolve beam origin and true word ids.
        topk_beam_index = topk_ids.div(vocab_size)
        topk_ids = topk_ids.fmod(vocab_size)

        # Map beam_index to batch_index in the flat representation.
        batch_index = (
            topk_beam_index
            + beam_offset[:topk_beam_index.size(0)].unsqueeze(1)
        )
        select_indices = batch_index.view(-1)

        # Append last prediction.
        alive_seq = torch.cat(
            [alive_seq.index_select(0, select_indices),
             topk_ids.view(-1, 1)],
            -1
        )

        is_finished = topk_ids.eq(symbols['EOS'])
        if step + 1 == PRESUMM_ARGS.max_length:
            is_finished.fill_(1)

        # End condition is top beam is finished.
        end_condition = is_finished[:, 0].eq(1)

        # Save finished hypotheses.
        if is_finished.any():
            predictions = alive_seq.view(
                -1, PRESUMM_ARGS.beam_size, alive_seq.size(-1)
            )
            for i in range(is_finished.size(0)):
                b = batch_offset[i]
                if end_condition[i]:
                    is_finished[i].fill_(1)
                finished_hyp = is_finished[i].nonzero().view(-1)
                # Store finished hypotheses for this batch.
                for j in finished_hyp:
                    hypotheses[b].append((
                        topk_scores[i, j],
                        predictions[i, j, 1:]))
                # If the batch reached the end, save the n_best hypotheses.
                if end_condition[i]:
                    best_hyp = sorted(
                        hypotheses[b], key=lambda x: x[0], reverse=True)
                    score, pred = best_hyp[0]

                    results["scores"][b].append(score)
                    results["predictions"][b].append(pred)
            non_finished = end_condition.eq(0).nonzero().view(-1)
            # If all sentences are translated, no need to go further.
            if len(non_finished) == 0:
                break
            # Remove finished batches for the next step.
            topk_log_probs = topk_log_probs.index_select(0, non_finished)
            batch_index = batch_index.index_select(0, non_finished)
            batch_offset = batch_offset.index_select(0, non_finished)
            alive_seq = predictions.index_select(0, non_finished) \
                .view(-1, alive_seq.size(-1))

        # Reorder states.
        select_indices = batch_index.view(-1)
        src_features = src_features.index_select(0, select_indices)
        dec_states.map_batch_fn(
            lambda state, dim: state.index_select(dim, select_indices))

    del src_features, dec_states  # clear gpu memory

    return results
